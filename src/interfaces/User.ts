export default interface User {
  uuid: string;
  firstname: string;
  lastname: string;
  picture: string;
}
