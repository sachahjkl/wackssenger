export default interface Discussion {
  uuid: string;
  userUUIDs: string[];
  messageUUIDs: string[];
}
