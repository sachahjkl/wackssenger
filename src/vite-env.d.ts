/// <reference types="svelte" />
/// <reference types="vite/client" />
interface ImportMetaEnv {
  VITE_HOPEX_GRAPHQL_URL: string;
  VITE_ENV_ID: string;
  VITE_REPO_ID: string;
  VITE_USERNAME: string;
  VITE_PASSWORD: string;
  VITE_PROFILE_ID: string;
  VITE_CLIENT_SECRET: string;
  VITE_CLIENT_ID: string;
  VITE_REFRESH_TOKEN: string;
  VITE_TOKEN: string;
  VITE_HOPEX_TASK: string;
  VITE_DATASET_ID: string;
  VITE_DOCUMENT_ID: string;
  VITE_DIAGRAM_ID: string;
}
